QT += testlib widgets

CONFIG += c++11 console
CONFIG -= app_bundle

# Both TestQString.h and TestGui.h have a macro that expands into a main function. We cannot have
# 3 main functions, so we need to implement branching to select which one of the files gets included in the build.
# In "Projects" (wrench logo on the left), go to "Build Steps" and click "Details" in qmake, then add
# "-config testConsole" in the "additional arguments" text field if you want to choose the testConsole
# branch and test console level stuff, or
# "-config testGUI" to choose the testGUI branch and test GUI stuff, or
# leave the additional arguments empty to go to the last "else" branch and just run the regular main function.
testConsole {
    message(Configuring console test build...)
    HEADERS += \
        TestQString.h
} else {
    testGUI {
        message(Configuring GUI test build...)
        HEADERS += \
            TestGui.h
    } else {
        message(Configuring normal build...)
        SOURCES += \
                main.cpp
    }
}
