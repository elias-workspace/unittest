#include <QTest>

class TestQString: public QObject
{
    Q_OBJECT
private slots:
    void toUpper(); // We test a Qt library function toUpper() which uppercases characters.
};

void TestQString::toUpper()
{
    QString str = "Hello";
    QVERIFY(str.toUpper() == "HELLO"); // QVERIFY simply tests equality
    QCOMPARE(str.toUpper(), QString("HELLO")); // QCOMPARE also prints both values if they differ
}

QTEST_MAIN(TestQString) // QTEST_MAIN is a macro that expands into a main function.
