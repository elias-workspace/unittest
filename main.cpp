#include <QCoreApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    std::cout << "In regular main function, not testing anything!" << std::endl;

    return a.exec();
}
